# Changelog

All notable changes to [README] project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
## 0.1.5 - 2018-08-23
### Generate
- Declaration TS

## 0.1.4 - 2018-08-22
### Rewrite
- to TypeScript language

## 0.1.3 - 2018-08-15
### Added
- Multi files
- params: "isLowerCase", "replaceDot", "followExtensions"

[README]: README.md