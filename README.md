DK docs - convert .md (markdown) to .html
==============================================
 
## Install

Exec command

```sh
$ npm install --save dk-docs
```

## Docs

Example

```js
var dkDocs = require("dk-docs");
 
 dkDocs({
     files: [
         './README.en.md',
         './CHANGELOG.md',
         './README.ru.md',
     ],
     followExtensions: ["md"],
     isLowerCase: true,
     replaceDot: "_",
     outDir:"docs",
     nameArchive: "./dist/docs.zip",
 });
```
**Result**: [download][DOWNLOAD_ZIP]

## About

### Notes

* This project adheres to [Semantic Versioning][SEM_VER]

### License

[MIT][MIT]

[MIT]: https://opensource.org/licenses/mit-license.php
[SEM_VER]: https://semver.org/
[DOWNLOAD_ZIP]: dist/docs.zip
