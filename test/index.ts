import {DkDocs_opt, dkDocs_run} from "../src/main";

var opt: DkDocs_opt = {
    files           : [
        '../README.md',
    ],
    followExtensions: ["md"],
    isLowerCase     : true,
    replaceDot      : "_",
    nameArchive     : "../dist/docs.zip",
    archive         : {
        files: [],
        name : "",
    },
};

dkDocs_run(opt as DkDocs_opt);
