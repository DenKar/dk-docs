"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("../src/index");
var opt = {
    files: [
        '../README.md',
    ],
    followExtensions: ["md"],
    isLowerCase: true,
    replaceDot: "_",
    nameArchive: "../dist/docs.zip",
};
index_1.dkDocs_run(opt);
