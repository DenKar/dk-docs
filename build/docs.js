var {dkDocs_run} = require('../dist/js/src/main');

dkDocs_run({
    files: [
        '../README.md',
        '../CHANGELOG.md'
    ],
    followExtensions: ["md"],
    isLowerCase: true,
    replaceDot: "_",
    outDir: "../docs",
    nameArchive: "../dist/docs.zip"
});
