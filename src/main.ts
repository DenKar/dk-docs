import {dkDocs_html} from "./html";

export const dkDocs_run = dkDocs_html;

export type DkDocs_opt = {
    files?: Array<string>
    followExtensions?: Array<string>
    archive?:DkDocs_Archive
    nameArchive?: string
    isLowerCase?: boolean
    replaceDot?: string
    outDir?: string
}

export type DkDocs_Archive = {
    files: Array<string>
    name: string
}