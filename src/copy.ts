import {time_new, time_passedStd, Time_s} from "dk-time";
import * as fs from "fs";
import * as path from "path";
import {doc_mkDir} from "./mkdir";

const fileLs = [];

export const doc_copyPush = (mainDir, fileDir, ls) => {
    fileLs.push([mainDir, fileDir, ls]);
};

export const doc_copy = () => {
    console.log('=> Copy files: %s', fileLs.length);

    let c = 0;

    for (let j = 0; j < fileLs.length; j++) {
        const mainDir = fileLs[j][0];

        let fileDir = fileLs[j][1];

        const ls = fileLs[j][2];

        if (!fileDir) {
            fileDir = __dirname + '/../../../src/front/';
        }

        for (let i = 0; i < ls.length; i++) {
            c++;

            const time: Time_s = time_new(undefined);
            const file         = ls[i];

            doc_mkDir(path.dirname(mainDir + file), {isRelativeToScript: true});

            let ok = "ok";

            try {
                // fixme http
                if ((fileDir + file) !== mainDir + file) {
                    fs["copyFileSync"](fileDir + file, mainDir + file);
                }
            } catch (e) {
                ok = "err";
            }

            const passedStr = time_passedStd(time);

            console.log('#%s - %s %s (%s)', c, passedStr, file, ok);
        }
    }

    console.log('=> Copied files: %s', c);
};
