export const docs_toc = (html) => {
    let str = "";

    const found = html.match(/\<h(\d).*<\/h\d\>/g, "#");

    if (!found) {
        return;
    }


    for (let i = 0; i < found.length; i++) {
        const item = found[i].match(/\<h(\d).*\>(.*)<\/h\d\>/, "#");

        if (item[1] == 1) {
            continue;
        }

        str += item[2] + "<br>"
    }

    return str;
};
