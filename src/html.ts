import * as marked from "marked";
import * as archiver from "archiver";
import * as fs from "fs";
import * as path from "path";
import {doc_copy, doc_copyPush} from "./copy";
import {doc_links} from "./links";
import {DkDocs_opt} from "./main";
import {time_new, time_passedStd, Time_s} from "dk-time";
import {doc_mkDir} from "./mkdir";
import {docs_toc} from "./toc";

export const dkDocs_html = (opt: DkDocs_opt) => {
    if (typeof opt !== "object") {
        return;
    }

    const time: Time_s = time_new(undefined);
    const dirName      = require('path').dirname(require.main.filename) + "/";
    const baseDir      = fs.realpathSync(dirName + "../") + "/";

    let mainDir = baseDir + "docs/";

    if (opt.outDir) {
        mainDir = dirName + opt.outDir;
    }

    doc_mkDir(mainDir, {isRelativeToScript: true});

    mainDir = fs.realpathSync(mainDir) + "/";

    var css2 = "css/github.css";
    var js2  = "js/main.js";

    doc_mkDir(mainDir + "css", {isRelativeToScript: true});
    doc_mkDir(mainDir + "js", {isRelativeToScript: true});

    doc_copyPush(mainDir, undefined, [css2, js2]);

    if (typeof opt.files !== "object") {
        opt.files = [opt.files];
    }

    if (typeof opt.followExtensions !== "object") {
        opt.followExtensions = [opt.followExtensions];
    }

    opt.nameArchive = opt.nameArchive || "";
    opt.archive     = {
        name : dirName + opt.nameArchive,
        files: [],
    };

    let ok = 0;

    console.log("\r\n=== Convert files: %s (all)", opt.files.length);

    for (let i = 0; i < opt.files.length; i++) {
        const time: Time_s = time_new(undefined);

        let fileName = opt.files[i];
        let name     = path.basename(fileName, ".md");
        let renderer = new marked.Renderer();

        let dirFile = fs.realpathSync(
            dirName + path.dirname(fileName),
        ) + "/";

        let src = fs.readFileSync(dirFile + name + ".md", 'utf8');

        src = doc_links(mainDir, dirFile, src);

        var html = marked(src, {renderer: renderer});

        html = html.replace(/#markdown-header-/g, "#");

        var toc = docs_toc(html);

        tmp2(name);
        tmp();

        let cName = name;

        if (opt.isLowerCase) {
            cName = cName.toLowerCase();
        }

        if (opt.replaceDot !== "") {
            cName = cName.replace(/\./g, opt.replaceDot);
        }

        if (opt.outDir) {
            dirFile = mainDir;
        }

        cName = dirFile + cName + ".html";

        if (!opt.outDir && dirFile !== mainDir) {
            opt.archive.files.push(cName);
        }

        fs.writeFileSync(cName, html);

        ok++;

        const passedStr = time_passedStd(time);

        console.log("#%s - %s %s (ok)", ok, passedStr, name);
    }

    console.log('=> Converted files: %s (ok)\n', ok);

    doc_copy();

    makeArchive(opt.archive, baseDir, mainDir);

    function tmp2(fileName) {
        for (var i = 0; i < opt.followExtensions.length; i++) {
            var extension = opt.followExtensions[i];
            var re1       = new RegExp("href=\"(.*)." + extension + "\"", "g");

            html = html.replace(re1, function (all, str) {
                if (opt.isLowerCase) {
                    str = str.toLowerCase();
                }

                if (opt.replaceDot !== "") {
                    str = str.replace(/\./, opt.replaceDot);
                }

                return "href=\"" + str + ".html\"";
            });
        }
    }

    function tmp() {
        html = '<!DOCTYPE html>\n' +
            '\n' +
            '<html dir="ltr">' +
            '<head>' +
            '<link rel="stylesheet" type="text/css" href="' + css2 + '">' +
            '</head>' +
            '<body>' + /*toc + */html +
            '<script src="' + js2 + '"></script>' +
            '</body>' +
            '</html>';
    }

    const passedStr = time_passedStd(time);

    console.log('=> End - %s', passedStr);
};

function makeArchive(opt, baseDir, mainDir) {
    // require modules

    var output  = fs.createWriteStream(opt.name);
    var archive = archiver('zip', {
        zlib: {level: 9}, // Sets the compression level.
    });

    output.on('close', function () {
        console.log("\r\n" + archive.pointer() + ' total bytes');
    });

    output.on('end', function () {
        console.log('Data has been drained');
    });

    archive.on('warning', function (err) {
        if (err.code === 'ENOENT') {
            // log warning
        } else {
            // throw error
            throw err;
        }
    });

    archive.on('error', function (err) {
        throw err;
    });

    archive.pipe(output);

    if (typeof opt.files !== "object") {
        opt.files = [opt.files];
    }

    let dirName = require('path').dirname(require.main.filename) + "/";

    for (var i = 0; i < opt.files.length; i++) {
        const name    = opt.files[i];
        const newName = name.replace(baseDir, '');

        archive.file(name, {name: newName});
    }

    archive.directory(mainDir, "docs");
    archive.finalize();
}
