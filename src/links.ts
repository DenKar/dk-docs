import {time_new, time_passedStd, Time_s} from "dk-time";
import * as fs from "fs";
import {doc_copyPush} from "./copy";

export const doc_links = (mainDir, dirFile, data) => {
    data = data.replace(/\!\[(.*)\]\((.*)\)/g, function (re, one, two) {

        doc_copyPush(mainDir, dirFile, [two]);

        // two += "?11";
        // "![" + one + "](" + two + ")"

        const rez = re;

        return rez;
    });


    return data;
};
