"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var marked = require("marked");
var archiver = require("archiver");
var fs = require("fs");
var path = require("path");
var copy_1 = require("./copy");
var links_1 = require("./links");
var dk_time_1 = require("dk-time");
var mkdir_1 = require("./mkdir");
var toc_1 = require("./toc");
exports.dkDocs_html = function (opt) {
    if (typeof opt !== "object") {
        return;
    }
    var time = dk_time_1.time_new(undefined);
    var dirName = require('path').dirname(require.main.filename) + "/";
    var baseDir = fs.realpathSync(dirName + "../") + "/";
    var mainDir = baseDir + "docs/";
    if (opt.outDir) {
        mainDir = dirName + opt.outDir;
    }
    mkdir_1.doc_mkDir(mainDir, { isRelativeToScript: true });
    mainDir = fs.realpathSync(mainDir) + "/";
    var css2 = "css/github.css";
    var js2 = "js/main.js";
    mkdir_1.doc_mkDir(mainDir + "css", { isRelativeToScript: true });
    mkdir_1.doc_mkDir(mainDir + "js", { isRelativeToScript: true });
    copy_1.doc_copyPush(mainDir, undefined, [css2, js2]);
    if (typeof opt.files !== "object") {
        opt.files = [opt.files];
    }
    if (typeof opt.followExtensions !== "object") {
        opt.followExtensions = [opt.followExtensions];
    }
    opt.nameArchive = opt.nameArchive || "";
    opt.archive = {
        name: dirName + opt.nameArchive,
        files: [],
    };
    var ok = 0;
    console.log("\r\n=== Convert files: %s (all)", opt.files.length);
    for (var i = 0; i < opt.files.length; i++) {
        var time_1 = dk_time_1.time_new(undefined);
        var fileName = opt.files[i];
        var name_1 = path.basename(fileName, ".md");
        var renderer = new marked.Renderer();
        var dirFile = fs.realpathSync(dirName + path.dirname(fileName)) + "/";
        var src = fs.readFileSync(dirFile + name_1 + ".md", 'utf8');
        src = links_1.doc_links(mainDir, dirFile, src);
        var html = marked(src, { renderer: renderer });
        html = html.replace(/#markdown-header-/g, "#");
        var toc = toc_1.docs_toc(html);
        tmp2(name_1);
        tmp();
        var cName = name_1;
        if (opt.isLowerCase) {
            cName = cName.toLowerCase();
        }
        if (opt.replaceDot !== "") {
            cName = cName.replace(/\./g, opt.replaceDot);
        }
        if (opt.outDir) {
            dirFile = mainDir;
        }
        cName = dirFile + cName + ".html";
        if (!opt.outDir && dirFile !== mainDir) {
            opt.archive.files.push(cName);
        }
        fs.writeFileSync(cName, html);
        ok++;
        var passedStr_1 = dk_time_1.time_passedStd(time_1);
        console.log("#%s - %s %s (ok)", ok, passedStr_1, name_1);
    }
    console.log('=> Converted files: %s (ok)\n', ok);
    copy_1.doc_copy();
    makeArchive(opt.archive, baseDir, mainDir);
    function tmp2(fileName) {
        for (var i = 0; i < opt.followExtensions.length; i++) {
            var extension = opt.followExtensions[i];
            var re1 = new RegExp("href=\"(.*)." + extension + "\"", "g");
            html = html.replace(re1, function (all, str) {
                if (opt.isLowerCase) {
                    str = str.toLowerCase();
                }
                if (opt.replaceDot !== "") {
                    str = str.replace(/\./, opt.replaceDot);
                }
                return "href=\"" + str + ".html\"";
            });
        }
    }
    function tmp() {
        html = '<!DOCTYPE html>\n' +
            '\n' +
            '<html dir="ltr">' +
            '<head>' +
            '<link rel="stylesheet" type="text/css" href="' + css2 + '">' +
            '</head>' +
            '<body>' + /*toc + */ html +
            '<script src="' + js2 + '"></script>' +
            '</body>' +
            '</html>';
    }
    var passedStr = dk_time_1.time_passedStd(time);
    console.log('=> End - %s', passedStr);
};
function makeArchive(opt, baseDir, mainDir) {
    // require modules
    var output = fs.createWriteStream(opt.name);
    var archive = archiver('zip', {
        zlib: { level: 9 },
    });
    output.on('close', function () {
        console.log("\r\n" + archive.pointer() + ' total bytes');
    });
    output.on('end', function () {
        console.log('Data has been drained');
    });
    archive.on('warning', function (err) {
        if (err.code === 'ENOENT') {
            // log warning
        }
        else {
            // throw error
            throw err;
        }
    });
    archive.on('error', function (err) {
        throw err;
    });
    archive.pipe(output);
    if (typeof opt.files !== "object") {
        opt.files = [opt.files];
    }
    var dirName = require('path').dirname(require.main.filename) + "/";
    for (var i = 0; i < opt.files.length; i++) {
        var name_2 = opt.files[i];
        var newName = name_2.replace(baseDir, '');
        archive.file(name_2, { name: newName });
    }
    archive.directory(mainDir, "docs");
    archive.finalize();
}
