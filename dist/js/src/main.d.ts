export declare const dkDocs_run: (opt: DkDocs_opt) => void;
export declare type DkDocs_opt = {
    files?: Array<string>;
    followExtensions?: Array<string>;
    archive?: DkDocs_Archive;
    nameArchive?: string;
    isLowerCase?: boolean;
    replaceDot?: string;
    outDir?: string;
};
export declare type DkDocs_Archive = {
    files: Array<string>;
    name: string;
};
