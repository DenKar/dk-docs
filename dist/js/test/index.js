"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var main_1 = require("../src/main");
var opt = {
    files: [
        '../README.md',
    ],
    followExtensions: ["md"],
    isLowerCase: true,
    replaceDot: "_",
    nameArchive: "../dist/docs.zip",
    archive: {
        files: [],
        name: "",
    },
};
main_1.dkDocs_run(opt);
